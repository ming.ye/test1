export default {
    methods: {
        getUrlParams(key) {
            const href = location.href;
            let params = href.split('?')[1] || '';
            const result = {};
            params = params.split('&');
            params.map(item => {
                const attr = item.split('=')[0];
                const value = item.split('=')[1];
                result[attr] = value;
            });
            if (key) {
                return result[key];
            } else {
                return result;
            }
        }
    }
};
